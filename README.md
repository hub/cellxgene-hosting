# Hosting CELLxGENE on Institut Pasteur's Kubernetes clusters

* Dev: https://aedes-cell-atlas.dev.pasteur.cloud/
* Prod: https://aedes-cell-atlas.pasteur.cloud/

To host a file, have it download-able from somewhere. We recommend using https://dl.pasteur.fr/.
As the file is downloaded at the first start of the CELLxGENE instance, and then kept, 
It does not matter if the file is not accessible after a short periode of time.


Let us considere that you want to serve `pbrc-4k-foo-bar.h5ad` using CELLxGENE without plugins,
served at https://aedes-cell-atlas.dev.pasteur.cloud/pbrc/. 
You should then create a new file ``pbrc.md`` in this directory with this content:
```yaml
---
ann_data_file : https://dl.pasteur.fr/fop/rpzyTOkP/pbrc-4k-foo-bar.h5ad
url_name :      pbrc
list_name :     PbRC data, following foo bar clustering
image_kind :    cellxgene
used_in_prod :  false
img_url:        oiseau-paradis-small.jpg
css_class:      dark
ram_needed:     512Mi
---

Here you write a markdown description of this file (required).
This will be shown in the main page.

```
Details on attributs:

* `ann_data_file` (required) link to the datafile
* `url_name` (required) the path to use in the url
* `list_name` (required) the list_name of the dataset in the main page
* `image_kind` (required) whether you ant simple cellxgene, or cellxgene with vip pluging. Choices: cellxgene, vip, cellxgeneNumpy126
* `sub_head` (optional, default: None) the sub_head of the dataset in the main page
* `used_in_prod` (optional, default: false) Do you want it to be available in the public website
* `img_url` (optional, default: None) filename in static/ or complet url to the image to be used as background
* `css_class` (optional, default: None) css theme to use for this dataset, usefull if image is dark.
* `ram_needed` (optional, default: 512Mi) ram needed whe  running cellxgene/vip with this dataset, use evaluate_ram_needed.sh to have a value


# How to stop serving a file

The simplest way is to delete the markdown file from this repository (``pbrc.md`` in our example).
This trigger the removal of the CELLxGENE instance serving the file, and the storage containing the file.

# How to update content in the main page

Content that is not defined by md file described herebefore, is to be found in web-content.yaml.
Here is an additional example, all field are to be considered require if not indicated explicitly optional.

```yaml
webContent:
  members:
  - firstName: Ada
    lastName: Lovelace
    title: Principal Investigator
    shortBio: Mathematics, computing
    webSite: https://en.wikipedia.org/wiki/Ada_Lovelace
    labWebSite: https://en.wikipedia.org/wiki/Ada_Lovelace#Work
    img: https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Ada_Byron_daguerreotype_by_Antoine_Claudet_1843_or_1850_-_cropped.png/220px-Ada_Byron_daguerreotype_by_Antoine_Claudet_1843_or_1850_-_cropped.png
    imgBackground: "" # (optional) can be an empty string, an image in static or an url. If not provided, title-bg.jpg or title-bg.png will be used.
  introArea:
    title: Aedes cell atlas
    introTitle: Welcome to our Aedes aegypti cell atlas !
    intro: The text to render bellow the intro title
    boxes:
    - title: Decode our code
      content: please add content in web-content.yaml. Keep it short and simple, and smile :)
      linkName: View more
      link: "/#cells" # can be an empty string, button is then disabled
```

The image used in the upper part of the page is the file title-bg.jpg or title-bg.png that can be found in static/.
This image is also used as default image in card for team member.
If the image is absent from static dir, a dark theme is used.


