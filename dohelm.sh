#!/usr/bin/env bash

if [ "$1" == "offline" ]; then
     OFFLINE=1
else
     OFFLINE=0
fi

echo $OFFLINE

touch tokens.sh
source ./tokens.sh # put `export SECRET_KEY="..."` in this file

NAMESPACE="cellxgene-dev"
CI_COMMIT_REF_SLUG=$(git branch --show | cut -c1-63 | sed 's/-*$//g')
REL_NAME=$CI_COMMIT_REF_SLUG
PLAUSIBLE_DATA_DOMAIN="aedes-cell-atlas.dev.pasteur.cloud"
PACKAGE_NAME="index-page-${NAMESPACE}"
PROD_ONLY=False

if [ $OFFLINE == 1 ]; then
     cp values-cellxgene.old.cached.yaml values-cellxgene.old.yaml
else
     helm get values ${CI_COMMIT_REF_SLUG} -o yaml > values-cellxgene.old.yaml || echo ""
fi

cellxgene-hosting helm-values \
--branch $CI_COMMIT_REF_SLUG \
--prod-only $PROD_ONLY \
--package-name $PACKAGE_NAME \
--plausible-data-domain $PLAUSIBLE_DATA_DOMAIN

if [ $OFFLINE == 1 ]; then
     cp static/* html/
     exit 0
fi

export ACTION="upgrade --install --wait"
# export ACTION="template --debug"

kubectl delete po -l"app.kubernetes.io/instance=${CI_COMMIT_REF_SLUG},sidekick=true"

helm ${ACTION} --namespace=${NAMESPACE} \
     --render-subchart-notes \
     --values ./chart/values.yaml \
     --values ./values-cellxgene.yaml \
     ${REL_NAME} ./chart/