---
ann_data_file : https://dl.pasteur.fr/fop/rpkyJqWP/pbmc3k.h5ad
url_name:       pbmc3k
list_name:      Small Test file (pbmc3k)
sub_head:       subhead lorem ipsum
image_kind:     cellxgeneNumpy126
css_class:      dark
img_url:       aedes-aegypti.jpg
used_in_prod:   false
---

This file is *~50Mo* and is only for tryout purpose
