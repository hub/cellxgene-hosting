---
ann_data_file : https://dl.pasteur.fr/fop/9EkRgPuu/M2-M10_16092024_colors.h5ad
url_name:       midgut
list_name:      Midgut
image_kind:     vip
img_url:        midgut.jpeg
css_class:      dark
used_in_prod:   true
ram_needed:     1.5Gi
---

Day 2 and 7 post blood meal