FROM continuumio/miniconda3

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update \
 && apt-get install -y \
        build-essential \
        libxml2-dev \
        python3-dev \
        python3-pip \
        zlib1g-dev \
        python3-requests \
        python3-aiohttp \
        curl \
        cpio \
 && rm -rf /var/lib/apt/lists/* \
 && python -m pip install --upgrade pip

RUN pip3 install cellxgene
    
#ARG VIP_VERSION=master
#RUN git clone -b $VIP_VERSION https://github.com/interactivereport/cellxgene_VIP.git /cellxgene_VIP
COPY cellxgene_VIP /cellxgene_VIP

RUN cd /cellxgene_VIP && \
    ./install_VIPlight.sh

RUN conda clean --all --yes

COPY pbmc3k.h5ad /pbmc3k.h5ad

EXPOSE 5005

#ENTRYPOINT ["cellxgene"]
#CMD ["cellxgene", "launch", "--host", "0.0.0.0", "/pbmc3k.h5ad"]
CMD ["/cellxgene_VIP/VIPlight", "launch", "--host", "0.0.0.0", "/pbmc3k.h5ad"]

