---
ann_data_file : https://dl.pasteur.fr/fop/fRan7VQp/F6-F14_240805.h5ad
url_name:       testfatbody
list_name:      FB other test
image_kind:     vip
img_url:        aedes-aegypti.jpg
css_class:      dark
used_in_prod:   false
ram_needed:     2.25Gi
---

This file is 323Mo and is for tryout purpose