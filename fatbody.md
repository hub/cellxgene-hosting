---
ann_data_file : https://dl.pasteur.fr/fop/nb55VznU/F6-F14_16092024_colors.h5ad
url_name:       fatbody
list_name:      Fat body
sub_head:       
image_kind:     vip
img_url:        fatbody.jpeg
css_class:      dark
used_in_prod:   true
ram_needed:     1.5Gi
---

Day 2 and 7 post blood meal 