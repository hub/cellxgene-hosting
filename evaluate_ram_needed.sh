#!/usr/bin/env bash

if [ -z $1 ]; then
  echo "You must provide a dataset name"
  exit 1
fi

if [ ! -e $1 ]; then
  echo "You must provide a dataset that actually exists"
  exit 1
fi

MD_FILE=$1

for UTILITY in jq wget cut rev python3 sed tail
do
  if [ ! "$(which $UTILITY 1>/dev/null; echo $?)" == "0" ]; then
    echo "$UTILITY must be installed for this script to work properly"
    echo "sudo apt-get install $UTILITY"
    echo "brew install $UTILITY"
    exit 2
  fi
done

if [ ! "$(which docker 1>/dev/null; echo $?)" == "0" ]; then
  echo "docker must be installed for this script to work properly"
  echo "https://docs.docker.com/engine/install/ubuntu/"
  echo "https://docs.docker.com/desktop/install/mac-install/"
  exit 2
fi

mkdir -p tmp-data/

URL="https:$(grep ann_data_file $MD_FILE | cut -d':' -f3)"
echo "URL for the file: $URL"

if [ "$URL" == "" ]; then
  echo "empty URL, exiting"
  exit 3
fi

FILENAME="$(echo -n $URL | rev | cut -d'/' -f-1 | rev)"

if [ "$FILENAME" == "" ]; then
  echo "Could not extract filename from URL, exiting"
  exit 4
fi

if [ -e tmp-data/$FILENAME ]; then
  echo "File already downloaded, using local copy"
else
  echo "Downloading file"
  if [ ! "$(wget $URL -O tmp-data/$FILENAME ; echo $?)" == "0" ]; then
    rm -f tmp-data/$FILENAME
    exit 5
  fi
fi

docker kill cx-try --quiet 2>/dev/null

docker run --quiet -d -v $(pwd)/tmp-data/:/data/ --name cx-try --rm registry-gitlab.pasteur.fr/hub/cellxgene-hosting/cellxgene-vip:latest launch --host 0.0.0.0 /data/$FILENAME --disable-annotations --disable-gene-sets-save

OLD_RAM_USED="Unknown"
while true; do
  RAM_USED=$(docker stats cx-try --no-stream --format json | jq '.MemUsage' | cut -d'/' -f1 | cut -d'"' -f2)
  echo "Current ram usage: $RAM_USED (waiting for stability)"
  [ "$OLD_RAM_USED" == "$RAM_USED" ] && break
  OLD_RAM_USED=$RAM_USED
  sleep 1
done

docker kill cx-try
RAM_NEEDED="$(python3 -c "print(round(float('${RAM_USED}'.lower().replace('g','').replace('m','').replace('i','').replace('b','').replace('o',''))*4/(1024 if 'M' in '${RAM_USED}' else 1))/4)")Gi"


echo "Ram used: $RAM_USED"
echo "If usage is more than 512Mi, you should indicate that it need more ram than the default value."
echo "To do so, add the following line in the header of the file:"
echo "---"
echo "ann_data_file: $URL"
echo "..."
echo "ram_needed: $RAM_NEEDED"
echo "---"

sed -i '/ram_needed/d' $MD_FILE

ROW_ID=$(grep -n "\-\-\-" ${MD_FILE} | tail -n 1 | cut -d':' -f1)

sed -i "$ROW_ID i ram_needed: $RAM_NEEDED" ${MD_FILE}